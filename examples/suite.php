<?php
use think\facade\Env;
return [
  //文件同步配置
  'sync' =>[
    
    // 'remote'=>'http://admin.tpl.ali2app.com/layadmin',
    'remote'=>'http://tien.ali2app.com/audit',
  ],
  'tcp' =>[
    'mode'=>'dev',
    'defalut'=>[
      'host' => '0.0.0.0',
      'port' => '9966',
      'pid_file' => '',
      'logs' => '',
    ],
    'dev'=>[
      'host' => '0.0.0.0',
      'port' => '9967',
      'pid_file' => '',
      'logs' => '',
    ],
  ],
  'udp' =>[
    'mode'=>'dev',
    'defalut'=>[
      'host' => '0.0.0.0',
      'port' => '9866',
      'pid_file' => '',
      'logs' => '',
    ],
    'dev'=>[
      'host' => '0.0.0.0',
      'port' => '9867',
      'pid_file' => '',
      'logs' => '',
    ],
  ],
  'http' =>[
    'mode'=>'dev',
    'watch'=> [
      Env::get('app_path').'base/controller',
    ],
    'defalut'=>[
      'host' => '0.0.0.0',
      'port' => '8800',
      'pid_file' => '',
      'logs' => '',
    ],
    'dev'=>[
      'host' => '0.0.0.0',
      'port' => '8801',
      'pid_file' => '',
      'logs' => '',
    ],
  ],
  'websocket' =>[
    'mode'=>'dev',
    'defalut'=>[
      'host' => '0.0.0.0',
      'port' => '8800',
      'pid_file' => '',
      'logs' => '',
    ],
    'dev'=>[
      'host' => '0.0.0.0',
      'port' => '8801',
      'pid_file' => '',
      'logs' => '',
    ],
  ]

];
