<?php
use think\facade\Env;
return [
  'database'=>'crontab',
  'run_name'=>'suite',
  'pid_file'=>'',
  'logs_path'  =>  Env::get('runtime_path'),
  'tasks' =>  [
    [
      'type'  =>  'class',
      'name'=>  '台州-打包传输', 
      'cron'=>  '01 * * * * *', 
      'url'=>  'index/controller/Taizhou/zip',
      'args'  =>  '',
    ],
    [
      'type'  =>  'remote',
      'name'=>  0, 
      'url'=>  'lps-htp',
      'args'  =>  '',
    ],
  ],
];
