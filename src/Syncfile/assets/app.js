//base
(function($){
  jQuery.app = {
    post:function(options){
      var defaults = {
        url:'',
        data:{},
        success:function(code,msg,r,data){
          //
        },
        error:function(r){
          //
        },
      };
      var setting = $.extend({}, defaults, options);

      $.post(setting.url,setting.data,function(r){
        if (r.code == 200 || r.code == 0) {
          setting.success(r.code,r.msg,r.data,r);
        }else{
          setting.error(r);
        }
      })
    },
  };
})(jQuery);

