<?php
Route::post('suite/syncfile/down', "\\Ayhome\\Suite\\Syncfile\\Index@down");
Route::post('suite/syncfile/up', "\\Ayhome\\Suite\\Syncfile\\Index@up");

Route::get('suite/syncfile/index', "\\Ayhome\\Suite\\Syncfile\\Index@index");
Route::post('suite/syncfile/sync', "\\Ayhome\\Suite\\Syncfile\\Index@sync");
// 注册命令行指令
\think\Console::addDefaultCommands([
  '\\Ayhome\\Suite\\Command\\Queue',
  '\\Ayhome\\Suite\\Command\\Tcp',
  '\\Ayhome\\Suite\\Command\\Crontab',
  '\\Ayhome\\Suite\\Command\\Udp',
  '\\Ayhome\\Suite\\Command\\WebSocket',
  '\\Ayhome\\Suite\\Command\\Http',
  '\\Ayhome\\Suite\\Command\\Task',
]);

function format_byte($byte)
{
  $KB = 1024;
  $MB = 1024 * $KB;
  $GB = 1024 * $MB;
  $TB = 1024 * $GB;
  if ($byte < $KB) {
    return $byte . "B";
  } elseif ($byte < $MB) {
    return round($byte / $KB, 2) . "KB";
  } elseif ($byte < $GB) {
    return round($byte / $MB, 2) . "MB";
  } elseif ($byte < $TB) {
    return round($byte / $GB, 2) . "GB";
  } else {
    return round($byte / $TB, 2) . "TB";
  }
}
