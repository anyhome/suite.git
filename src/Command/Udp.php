<?php
namespace Ayhome\Suite\Command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Config;
use think\facade\Env;
use Ayhome\Suite\Server\Udp as UdpServer;

class Udp extends Command
{
  protected $config = [];
  protected $server;
  protected $daemonize = false;

  public function configure()
  {
      $this->setName('udp')
          ->addArgument('action', Argument::OPTIONAL, "start|stop|restart|reload", 'start')
          ->addOption('daemon', 'd', Option::VALUE_NONE, 'Run the swoole server in daemon mode.')
          ->setDescription('Swoole Process for ThinkPHP');
  }

  public function execute(Input $input, Output $output)
  {


    $action = $input->getArgument('action');

    $cfg = config('suite.');
    $this->config = $cfg['udp'];

    if ($this->input->hasOption('daemon')) {
      $this->daemonize = true;
    }

    $this->init();
    if (in_array($action, ['start', 'stop', 'reload', 'restart','status'])) {
      $this->$action();
    } else {
      $output->writeln("<error>Invalid argument action:{$action}, Expected start|stop|restart|reload .</error>");
    }
  }

  public function init()
  {
    $defalut = $this->config['defalut'];
    if (empty($defalut['pid_file'])) {
      $defalut['pid_file'] = Env::get('runtime_path') . 'suite-udp.pid';
    }
    if (empty($defalut['log_file'])) {
      $defalut['log_file'] = Env::get('runtime_path') . 'suite-udp.log';
    }


    if (empty($defalut['host'])) $defalut['host'] = '0.0.0.0';
    if (empty($defalut['port'])) $defalut['port'] = '9866';

    $uri = "udp://{$defalut['host']}:{$defalut['port']}";
    $this->server = new UdpServer('suite:udp', $uri);

    $swoole = [
      "pid_file" =>$defalut['pid_file'],
      "daemonize" => $this->daemonize,
      "log_file" => $defalut['log_file'],

    ];

    $this->server->configure($swoole);

    if ($this->config['mode'] == 'debug') {
      $dev = $this->config['dev'];
      if (empty($dev['pid_file'])) {
        $dev['pid_file'] = Env::get('runtime_path') . 'suite-udp-dev.pid';
      }
      if (empty($dev['host'])) $dev['host'] = '0.0.0.0';
      if (empty($dev['port'])) $dev['port'] = '9867';
    }
  }

  public function start()
  {
    if ($this->config['watch'] && !$this->server->isRunning()) {
      $this->server->watch($this->config['watch']);
    }
    $this->server->start();
    return;
  }


  public function stop($value='')
  {
    $this->server->shutdown();
    return;
  }

  public function reload($value='')
  {
    $this->server->reload();
    // return;
  }

  public function status($value='')
  {
    $this->server->status();
    return;
  }

  public function restart($value='')
  {
    $this->server->restart();
    // return;
  }




}
