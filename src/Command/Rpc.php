<?php
namespace Ayhome\Suite\Command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Config;
use think\facade\Env;


class Rpc extends Command
{
  protected $config = [];

  public function configure()
  {
      $this->setName('suite:process')
          ->addArgument('action', Argument::OPTIONAL, "start|stop|restart|reload", 'start')
          ->addOption('daemon', 'd', Option::VALUE_NONE, 'Run the swoole server in daemon mode.')
          ->setDescription('Swoole Process for ThinkPHP');
  }

  public function execute(Input $input, Output $output)
  {
      $action = $input->getArgument('action');

      $this->init();

      if (in_array($action, ['start', 'stop', 'reload', 'restart'])) {
          $this->$action();
      } else {
          $output->writeln("<error>Invalid argument action:{$action}, Expected start|stop|restart|reload .</error>");
      }
  }

  protected function init()
  {
      $this->config = Config::pull('swoole');

      if (empty($this->config['pid_file'])) {
          $this->config['pid_file'] = Env::get('runtime_path') . 'suite-process.pid';
      }

      // 避免pid混乱
      $this->config['pid_file'] .= '_' . $this->getPort();
  }

}
