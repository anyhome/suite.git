<?php
namespace Ayhome\Suite\Server;


use swoole_server;

class Udp extends \FastD\Swoole\Server\UDP
{

  public function doPacket(swoole_server $server, $data, $clientInfo)
  {
    if ('quit' === $data) {
      $server->close($fd);
      return 0;
    }elseif ('quit' === $data) {
      return json_encode($server->stats());
    }
    $data =  json_decode($data, true);
    $url = $data['url'];
    if (!$url) {
      $server->close($fd);
      return 0;
    }
    $path_arr = explode("/", $url);
    $action = $path_arr[count($path_arr) - 1];
    $cls = $path_arr[2].'/'.$path_arr[3];

    $post = $data['post'];
    request()->raw = json_encode($data['data']);
    try {
      $ret = action($cls, $post, $path_arr[1].'\controller');
      $r = $ret->getContent();
      // print_r($r);

      $server->sendto($clientInfo['address'], $clientInfo['port'], $r);
    } catch (Exception $e) {
      $server->sendto($clientInfo['address'], $clientInfo['port'], $e->getMessage());
    }

    return 0;
  }

  public function doTask(swoole_server $server, $data, $taskId, $workerId)
  {
    echo $data . ' on task' . PHP_EOL;
    return $data;
  }

 
}
