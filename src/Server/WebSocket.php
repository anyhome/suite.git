<?php
namespace Ayhome\Suite\Server;


use swoole_server;
use swoole_websocket_server;
use swoole_http_request;
use swoole_http_response;
use swoole_websocket_frame;
class WebSocket extends \FastD\Swoole\Server\WebSocket
{
  public function doOpen(swoole_websocket_server $server, swoole_http_request $request)
  {
    echo "server: handshake success with fd{$request->fd}\n";
  }


  public function doMessage(swoole_server $server, swoole_websocket_frame $frame)
  {
    if ('quit' === $data) {
      $server->close($fd);
      return 0;
    }
    $data =  json_decode($data, true);
    $url = $data['url'];
    if (!$url) {
      $server->close($fd);
      return 0;
    }
    $path_arr = explode("/", $url);
    $action = $path_arr[count($path_arr) - 1];
    $cls = $path_arr[1].'/'.$path_arr[3];

    $post = $data['post'];
    try {
      $ret = action($cls, $post, $path_arr[2].'\controller');
      $r = $ret->getContent();
      // print_r($r);
      $server->push($frame->fd, $r);

    } catch (Exception $e) {
    }

    return 0;
  }


 
}
